# LISEZ-MOI

### Pourquoi ce dépôt ?

- Pour contenir quelques exemples de code

# Prérequis

Ce projet utilise *Groovy* et *Gradle*. Ces deux logiciels doivent être
préinstallés et paramétrés correctement.
