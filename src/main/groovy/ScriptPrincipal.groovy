
// Script principal

println "ScriptPrincipal : début de l'exécution\n"

println "\nTest du patron décorateur"

import patterns.structuration.decorateur.*

def cafeRegulier = new CafeRegulier()

println cafeRegulier

def avecCreme = new GarnitureCreme(cafeRegulier)

println avecCreme

def avecSmarties = new GarnitureSmarties(avecCreme)

println avecSmarties

def avecDoubleSmarties = new GarnitureSmarties(avecSmarties)

println avecDoubleSmarties

println "\nTest du patron état"

import patterns.structuration.etat.*

Compte compte = new Compte("007")

println compte

compte.deposer 50.00

println compte

compte.deces()

compte.deposer 50.00

println compte

println "\nScriptPrincipal : fin de l'exécution"
