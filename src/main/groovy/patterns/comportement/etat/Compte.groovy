package patterns.structuration.etat

class Compte implements CompteTransaction {

    String numero

    BigDecimal solde = 0.00

    CompteEtat etat

    Compte(String numero) {
        this.numero = numero
        etat = new CompteEtatRegulier(this)
    }

    def deposer(BigDecimal montant) { etat.deposer montant }

    def retirer(BigDecimal montant) { etat.retirer montant }

    def deces() { etat = new CompteEtatDeces(this) }

    def restreint() { etat = new CompteEtatRestreint(this) }

    String toString() { """
        Compte numéro : $numero
        Solde : ${java.text.NumberFormat.currencyInstance.format solde}
        """.stripIndent()
    }

}
