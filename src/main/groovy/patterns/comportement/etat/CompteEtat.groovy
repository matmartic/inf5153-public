package patterns.structuration.etat

abstract class CompteEtat implements CompteTransaction {

    final Compte compte

    CompteEtat(Compte compte) {
        this.compte = compte
    }

    def deposer(BigDecimal montant) {
        compte.solde += montant
    }

    def retirer(BigDecimal montant) {
        compte.solde -= montant
    }

}
