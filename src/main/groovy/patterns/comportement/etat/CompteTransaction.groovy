package patterns.structuration.etat

interface CompteTransaction {

    def deposer(BigDecimal montant)

    def retirer(BigDecimal montant)

}
