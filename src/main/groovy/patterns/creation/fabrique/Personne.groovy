package patterns.creation.fabrique

abstract class Personne {

    final String uuid = UUID.randomUUID().toString()

    abstract String getNom()

    boolean estPersonneMorale() { this instanceof PersonneMorale }

    boolean estPersonnePhysique() { this instanceof PersonnePhysique }

    String toString() {
        "${this.class.simpleName}\n\tUUID : $uuid\n\tNom : $nom\n\t"
    }

}
