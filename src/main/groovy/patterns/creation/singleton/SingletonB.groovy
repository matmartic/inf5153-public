package patterns.creation.singleton

enum SingletonB {

    INSTANCE

    String toString() { "Je suis un singletonB " + this.hashCode() }

}
