package patterns.structuration.decorateur

class CafeCorse implements Cafe {

    {
        description = "Café corsé"
        prix = 1.75
    }

}
