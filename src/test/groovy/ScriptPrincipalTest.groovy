// Script principal de test

import spock.lang.Specification

class ScriptPrincipalTest extends Specification {

    def "Test du vrai pour fin de démonstration"() {
        given: "La variable 'vrai'"
            def vrai
        when: "Sa valeur est initialisée à 'true'"
            vrai = true
        then: "Le résultat est positif"
            vrai == true
    }

    def "Test du faux pour fin de démonstration"() {
        given: "La variable 'faux'"
            def faux
        when: "Sa valeur est initialisée à 'false'"
            faux = false
        then: "Le résultat est positif"
            faux == false
    }

}
