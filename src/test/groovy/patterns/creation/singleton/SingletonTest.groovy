package patterns.creation.singleton

import spock.lang.Specification

class SingletonTest extends Specification {

    def "Tester un singleton créé avec l'annotation @Singleton"() {
        given: "Plusieurs références au singleton"
            def singleton1 = SingletonA.instance
            def singleton2 = SingletonA.instance
            def singleton3 = SingletonA.instance
        expect: "Chaque référence pointe vers le même objet"
            singleton1.is singleton2
            singleton2.is singleton3
            singleton1.is singleton3
    }

    def "Tester un singleton créé avec une énumération"() {
        given: "Plusieurs références au singleton"
            def singleton1 = SingletonB.INSTANCE
            def singleton2 = SingletonB.INSTANCE
            def singleton3 = SingletonB.INSTANCE
        expect: "Chaque référence pointe vers le même objet"
            singleton1.is singleton2
            singleton2.is singleton3
            singleton1.is singleton3
    }

}
